package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;

	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}

	/**
	 * Returns the node at the specified position in this list.
	 *
	 * @param index index of the node to return
	 * @return the node at the specified position in this list
	 * @throws IndexOutOfBoundsException if the index is out of range
	 *         ({@code index < 0 || index >= size()})
	 */
	private Node<T> getNode(int index) {
		if (n <= index || index < 0){
			throw new IndexOutOfBoundsException();
		}
		Node<T> nextObject = head;
		for (int i = 0; i < index; i++) {
			nextObject = nextObject.next;
		}
		return nextObject;
	}

	@Override
	public void add(int index, T element) {
		if (n < index || index < 0) {
			throw new IndexOutOfBoundsException();
		}
		Node<T> eNode = new Node<>(element);//
		if (index == 0){
			eNode.next = head;
			head = eNode;
		}
		else{
			Node<T> nodeI = getNode(index - 1);
			eNode.next = nodeI.next;
			nodeI.next = eNode;
		}
		n++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}

}