package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {


	public static final int DEFAULT_CAPACITY = 10;

	private int n;

	private Object elements[];

	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}

	@Override
	public T get(int index) {
		if (index < 0 && index >= n){
			throw new IndexOutOfBoundsException();
		}
		return (T) elements[index];
	}

	@Override
	public void add(int index, T element) {
		if (n >= elements.length){
			ensureCapacity();
		}

		for (int i = n - 1; i >= index; i--) {
			elements[i + 1] = elements[i];
		}
		elements[index] = element;
		n++;
	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

	public void ensureCapacity() {
		int dobSize = elements.length * 2;
		Object newEl[] = new Object[dobSize];
		for(int i = 0 ; i < elements.length ; i++){
			newEl[i] = elements[i];
		}
		elements = newEl;
	}

}